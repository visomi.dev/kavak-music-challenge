const express = require('express');

const schemaValidator = require('../middlewares/schemaValidator');
const paginated = require('../middlewares/paginated');
const idParam = require('../middlewares/idParam');

const albums = require('../usecases/albums');

const createAlbumSchema = require('../docs/albums/post-request-schema.json');
const getAlbumsSchema = require('../docs/albums/get-request-schema.json');
const getAlbumSchema = require('../docs/albums/id/get-request-schema.json');
const updateAlbumSchema = require('../docs/albums/id/patch-request-schema.json');
const deleteAlbumSchema = require('../docs/albums/id/delete-request-schema.json');

const router = express.Router();

router.post(
  '/',
  schemaValidator(createAlbumSchema),
  async (request, response) => {
    try {
      const data = await albums.createOne(request.body);

      response.json({
        success: true,
        message: 'Album created',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/',
  paginated,
  (request, response, next) => {
    if (request.query.id) request.query.id = parseInt(request.query.id, 10);
    if (request.query.userId) request.query.userId = parseInt(request.query.userId, 10);
    if (request.query.upc) request.query.upc = parseInt(request.query.upc, 10);
    if (request.query.status) request.query.status = JSON.parse(request.query.status);

    next();
  },
  schemaValidator(getAlbumsSchema, { type: 'query' }),
  async (request, response) => {
    try {
      if (request.query.status !== undefined) request.query.status = request.query.status ? 1 : 0;

      const data = await albums.findAll(request.query);

      response.json({
        success: true,
        message: 'Albums found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/:id(\\d+)',
  idParam,
  schemaValidator(getAlbumSchema, { type: 'params' }),
  async (request, response) => {
    try {
      const data = await albums.findById(request.params.id);

      response.json({
        success: true,
        message: 'Album found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.patch(
  '/:id(\\d+)',
  idParam,
  schemaValidator(updateAlbumSchema),
  async (request, response) => {
    try {
      const data = await albums.updateById(request.params.id, request.body);

      response.json({
        success: true,
        message: 'Album updated',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.delete(
  '/:id(\\d+)',
  idParam,
  schemaValidator(deleteAlbumSchema, { type: 'params' }),
  async (request, response) => {
    try {
      await albums.deleteById(request.params.id);

      response.json({
        success: true,
        message: 'Album deleted',
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

module.exports = router;
