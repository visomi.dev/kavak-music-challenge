const express = require('express');

const schemaValidator = require('../middlewares/schemaValidator');
const paginated = require('../middlewares/paginated');
const idParam = require('../middlewares/idParam');

const users = require('../usecases/users');

const createUserSchema = require('../docs/users/post-request-schema.json');
const getUsersSchema = require('../docs/users/get-request-schema.json');
const getUserSchema = require('../docs/users/id/get-request-schema.json');
const updateUserSchema = require('../docs/users/id/patch-request-schema.json');
const deleteUserSchema = require('../docs/users/id/delete-request-schema.json');

const router = express.Router();

router.post(
  '/',
  schemaValidator(createUserSchema),
  async (request, response) => {
    try {
      const data = await users.createOne(request.body);

      response.json({
        success: true,
        message: 'User created',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/',
  paginated,
  (request, response, next) => {
    if (request.query.id) request.query.id = parseInt(request.query.id, 10);
    if (request.query.countryId) request.query.countryId = parseInt(request.query.countryId, 10);
    if (request.query.status) request.query.status = JSON.parse(request.query.status);

    next();
  },
  schemaValidator(getUsersSchema, { type: 'query' }),
  async (request, response) => {
    try {
      if (request.query.status !== undefined) request.query.status = request.query.status ? 1 : 0;

      const data = await users.findAll(request.query);

      response.json({
        success: true,
        message: 'Users found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/:id(\\d+)',
  idParam,
  schemaValidator(getUserSchema, { type: 'params' }),
  async (request, response) => {
    try {
      const data = await users.findById(request.params.id);

      response.json({
        success: true,
        message: 'User found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.patch(
  '/:id(\\d+)',
  idParam,
  schemaValidator(updateUserSchema),
  async (request, response) => {
    try {
      const data = await users.updateById(request.params.id, request.body);

      response.json({
        success: true,
        message: 'User updated',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.delete(
  '/:id(\\d+)',
  idParam,
  schemaValidator(deleteUserSchema, { type: 'params' }),
  async (request, response) => {
    try {
      await users.deleteById(request.params.id);

      response.json({
        success: true,
        message: 'User deleted',
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

module.exports = router;
