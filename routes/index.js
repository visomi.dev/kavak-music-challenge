const path = require('path');
const express = require('express');

const usecases = require('../usecases');

const router = express.Router();

router.get('/doc', (request, response, next) => {
  const options = {
    root: path.join(path.dirname(__dirname), 'docs'),
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true,
    },
  };

  response.sendFile('api.html', options, (err) => {
    if (err) next(err);
  });
});

router.get('/', async (request, response) => {
  const { page = 1 } = request.query;

  const data = await usecases.tracksWithAlbumAndUser(request.query);

  response.render('index', {
    title: 'Kavak Music Challenge',
    subtitle: 'Tracks with Album and User',
    tracks: data.rows,
    page,
    pages: data.pages,
  });
});

router.get('/albums-from-peru', async (request, response) => {
  const { page = 1, size = 20 } = request.query;

  const data = await usecases.albumsFromPeru({ page, size });

  response.render('albums-from-peru', {
    title: 'Kavak Music Challenge',
    subtitle: 'Albums from Peru',
    albums: data.rows,
    page,
    pages: data.pages,
  });
});

router.get('/empty-albums', async (request, response) => {
  const { page = 1, size = 20 } = request.query;

  const data = await usecases.getEmptyAlbums({ page, size });

  response.render('empty-albums', {
    title: 'Kavak Music Challenge',
    subtitle: 'Empty Albums',
    albums: data.rows,
    page,
    pages: data.pages,
  });
});

router.get('/empty-users', async (request, response) => {
  const { page = 1, size = 20 } = request.query;

  const data = await usecases.getEmptyUsers({ page, size });

  response.render('empty-users', {
    title: 'Kavak Music Challenge',
    subtitle: 'Empty Users',
    users: data.rows,
    page,
    pages: data.pages,
  });
});

router.get('/tracks-genres', async (request, response) => {
  const { page = 1, size = 20 } = request.query;

  const data = await usecases.tracksGenres({ page, size });

  response.render('tracks-genres', {
    title: 'Kavak Music Challenge',
    subtitle: 'Tracks Genres',
    tracks: data.rows,
    page,
    pages: data.pages,
  });
});

module.exports = router;
