const express = require('express');

const schemaValidator = require('../middlewares/schemaValidator');
const paginated = require('../middlewares/paginated');
const idParam = require('../middlewares/idParam');

const tracks = require('../usecases/tracks');

const createTrackSchema = require('../docs/tracks/post-request-schema.json');
const getTracksSchema = require('../docs/tracks/get-request-schema.json');
const getTrackSchema = require('../docs/tracks/id/get-request-schema.json');
const updateTrackSchema = require('../docs/tracks/id/patch-request-schema.json');
const deleteTrackSchema = require('../docs/tracks/id/delete-request-schema.json');

const router = express.Router();

router.post(
  '/',
  schemaValidator(createTrackSchema),
  async (request, response) => {
    try {
      const data = await tracks.createOne(request.body);

      response.json({
        success: true,
        message: 'Track created',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/',
  paginated,
  (request, response, next) => {
    if (request.query.id) request.query.id = parseInt(request.query.id, 10);
    if (request.query.userId) request.query.userId = parseInt(request.query.userId, 10);
    if (request.query.albumId) request.query.albumId = parseInt(request.query.albumId, 10);
    if (request.query.status) request.query.status = JSON.parse(request.query.status);

    next();
  },
  schemaValidator(getTracksSchema, { type: 'query' }),
  async (request, response) => {
    try {
      if (request.query.status !== undefined) request.query.status = request.query.status ? 1 : 0;

      const data = await tracks.findAll(request.query);

      response.json({
        success: true,
        message: 'Tracks found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/:id(\\d+)',
  idParam,
  schemaValidator(getTrackSchema, { type: 'params' }),
  async (request, response) => {
    try {
      const data = await tracks.findById(request.params.id);

      response.json({
        success: true,
        message: 'Track found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.patch(
  '/:id(\\d+)',
  idParam,
  schemaValidator(updateTrackSchema),
  async (request, response) => {
    try {
      const data = await tracks.updateById(request.params.id, request.body);

      response.json({
        success: true,
        message: 'Track updated',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.delete(
  '/:id(\\d+)',
  idParam,
  schemaValidator(deleteTrackSchema, { type: 'params' }),
  async (request, response) => {
    try {
      await tracks.deleteById(request.params.id);

      response.json({
        success: true,
        message: 'Track deleted',
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

module.exports = router;
