const express = require('express');

const schemaValidator = require('../middlewares/schemaValidator');
const paginated = require('../middlewares/paginated');
const idParam = require('../middlewares/idParam');

const countries = require('../usecases/countries');

const createCountrySchema = require('../docs/countries/post-request-schema.json');
const getCountriesSchema = require('../docs/countries/get-request-schema.json');
const getCountrySchema = require('../docs/countries/id/get-request-schema.json');
const updateCountrySchema = require('../docs/countries/id/patch-request-schema.json');
const deleteCountrySchema = require('../docs/countries/id/delete-request-schema.json');

const router = express.Router();

router.post(
  '/',
  schemaValidator(createCountrySchema),
  async (request, response) => {
    try {
      const data = await countries.createOne(request.body);

      response.json({
        success: true,
        message: 'Country created',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/',
  paginated,
  (request, response, next) => {
    if (request.query.id) request.query.id = parseInt(request.query.id, 10);

    next();
  },
  schemaValidator(getCountriesSchema, { type: 'query' }),
  async (request, response) => {
    try {
      const data = await countries.findAll(request.query);

      response.json({
        success: true,
        message: 'Countries found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.get(
  '/:id(\\d+)',
  idParam,
  schemaValidator(getCountrySchema, { type: 'params' }),
  async (request, response) => {
    try {
      const data = await countries.findById(request.params.id);

      response.json({
        success: true,
        message: 'Country found',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.patch(
  '/:id(\\d+)',
  idParam,
  schemaValidator(updateCountrySchema),
  async (request, response) => {
    try {
      const data = await countries.updateById(request.params.id, request.body);

      response.json({
        success: true,
        message: 'Country updated',
        data,
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

router.delete(
  '/:id(\\d+)',
  idParam,
  schemaValidator(deleteCountrySchema, { type: 'params' }),
  async (request, response) => {
    try {
      await countries.deleteById(request.params.id);

      response.json({
        success: true,
        message: 'Country deleted',
      });
    } catch (error) {
      response.status(error.status || 500);

      response.json({
        success: false,
        message: error.message,
        ...error.body,
      });
    }
  },
);

module.exports = router;
