const logger = require('./lib/logger');

const app = require('./app');

const { PORT = 8080 } = process.env;

async function main() {
  try {
    app.listen(PORT, () => {
      logger.info(`SERVER RUNNING AND LISTENING ON: ${PORT}`);
    });
  } catch (error) {
    logger.error(error);
  }
}

main();
