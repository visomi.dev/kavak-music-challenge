const mysql = require('mysql');

const {
  DB_HOST = 'localhost',
  DB_USER = 'kavak-music-challenge',
  DB_PASSWORD = 'kavak-music-challenge',
  DB_NAME = 'kavak_music_challenge',
} = process.env;

const connection = mysql.createConnection({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_NAME,
});

connection.connect();

module.exports = connection;
