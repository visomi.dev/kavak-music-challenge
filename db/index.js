const migrations = require('./migrations');
const seeds = require('./seeds');

const [,, command] = process.argv;

switch (command) {
  case 'migrate':
    migrations.up();
    break;
  case 'migrate:undo':
    migrations.down();
    break;
  case 'seed':
    seeds.up();
    break;
  case 'seed:undo':
    seeds.down();
    break;
  default:
    break;
}
