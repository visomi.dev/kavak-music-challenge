const connection = require('./connection');

const logger = require('../lib/logger');
const helpers = require('../lib/helpers');

const { LOG_LEVEL = 'debug' } = process.env;

const operators = {
  $equal: '=',
  $different: '!=',
  $like: 'LIKE',
  $rlike: 'RLIKE',
};

function queryToWhere(query = {}, join = 'AND') {
  const filters = Object.keys(query)
    .map((key) => {
      let value = query[key];

      let operator = operators.$equal;

      if (typeof value === 'object') {
        const {
          $equal,
          $different,
          $like,
          $rlike,
        } = value;

        if ($equal) {
          operator = operators.$equal;

          if (typeof $equal === 'string') value = `'${$equal}'`;
          else value = $equal;
        }

        if ($different) {
          operator = operators.$different;

          if (typeof $different === 'string') value = `'${$different}'`;
          else value = $different;
        }

        if ($like) {
          operator = operators.$like;

          if (typeof $like === 'string') value = `'${$like}'`;
          else value = $like;
        }

        if ($rlike) {
          operator = operators.$rlike;

          value = `'${$rlike}'`;
        }
      } else if (typeof value === 'string') {
        value = `'${value}'`;
      }

      return `${key} ${operator} ${value}`;
    })
    .join(` ${join} `);

  return filters;
}

function whereBuilder(filters = {}) {
  const { $or = {}, ...raw } = filters;

  const hasAnd = Object.keys(raw).length > 0;
  const hasOr = Object.keys($or).length > 0;

  if (!hasAnd && !hasOr) return '';

  let where = '';

  if (hasAnd) {
    const snakeRaw = helpers.snakeCaseObject(raw);

    where += queryToWhere(snakeRaw);
  }

  if (hasOr) {
    const snakeOr = helpers.snakeCaseObject($or);
    const or = queryToWhere(snakeOr, 'OR');

    if (hasAnd) where += ` AND (${or})`;
    else where += or;
  }

  return `WHERE ${where}`;
}

const query = (sql = '', params) => new Promise(((resolve, reject) => {
  if (LOG_LEVEL === 'debug') logger.debug(sql, params);

  connection.query(sql, params, (error, data) => {
    if (error) reject(error);
    else resolve(data);
  });
}));

const utils = {
  queryToWhere,
  whereBuilder,
  query,
};

module.exports = utils;
