const Model = require('.');

const TABLE = 'users';
const FIELDS = [
  'name',
  'email',
  'country_id',
  'status',
];

class User extends Model {}

const model = new User({
  table: TABLE,
  fields: FIELDS,
});

module.exports = model;
