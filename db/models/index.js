const { snakeCase } = require('lodash');

const dbutils = require('../utils');
const helpers = require('../../lib/helpers');

class Model {
  constructor(options = {}) {
    const { table = '', fields = [] } = options;

    this.table = table;
    this.$fields = fields;
  }

  fixFields(data = {}) {
    const fields = Object.keys(data).map(snakeCase);

    return fields
      .filter((field) => this.$fields.includes(field))
      .sort();
  }

  fixValues(data = {}) {
    const fields = Object.keys(data).map(snakeCase);

    return fields
      .filter((field) => this.$fields.includes(field))
      .sort()
      .map((key) => data[key]);
  }

  async createOne(data) {
    const fields = this.fixFields(data).join(', ');
    const fieldsTemplates = this.fixFields(data).map(() => '?').join(', ');

    const sql = `
      INSERT INTO ${this.table} (${fields})
      VALUES (${fieldsTemplates})
    `;

    const row = helpers.snakeCaseObject(data);
    const values = this.fixValues(row);

    const result = await dbutils.query(sql, values);

    return result;
  }

  async findOne(filters = {}) {
    let sql = `SELECT * FROM ${this.table}`;

    if (Object.keys(filters).length > 0) sql += ` ${dbutils.whereBuilder(filters)}`;

    sql += ' LIMIT 1';

    const [result] = await dbutils.query(sql);

    return helpers.camelCaseObject(result);
  }

  async count(filters = {}) {
    let sql = `SELECT count(*) as total FROM ${this.table}`;

    if (Object.keys(filters).length > 0) sql += ` ${dbutils.whereBuilder(filters)}`;

    const [result] = await dbutils.query(sql);

    return result.total;
  }

  async findAll(filters = {}, pagination = {}, sort = {}) {
    let sql = `SELECT * FROM ${this.table}`;

    const { size = 20, page = 1 } = pagination;
    const {
      fields: sortByFields = ['id'],
      dir: sortByDir = 'DESC',
    } = sort;

    const skip = (page - 1) * size;
    const limit = `${skip}, ${size}`;

    if (Object.keys(filters).length > 0) sql += ` ${dbutils.whereBuilder(filters)}`;

    sql += ` ORDER BY ${sortByFields} ${sortByDir} LIMIT ${limit}`;

    const results = await dbutils.query(sql);

    return helpers.snakeCaseArrayOfObjects(results);
  }

  async updateOne(filters = {}, data = {}) {
    if (Object.keys(filters).length === 0) {
      const error = new Error('Update one need one or more filters');

      throw error;
    }

    const fields = this.fixFields(data).map((field) => `${field} = ?`).join(', ');
    const where = dbutils.whereBuilder(filters);

    const sql = `
      UPDATE ${this.table}
      SET ${fields}
      ${where}
    `;

    const row = helpers.snakeCaseObject(data);
    const values = this.fixValues(row);

    const result = await dbutils.query(sql, values);

    return result;
  }

  async deleteOne(filters = {}) {
    if (Object.keys(filters).length === 0) {
      const error = new Error('Delete one need one or more filters');

      throw error;
    }

    const where = dbutils.whereBuilder(filters);

    const sql = ` DELETE FROM ${this.table} ${where}`;

    const result = await dbutils.query(sql);

    return result;
  }
}

module.exports = Model;
