const Model = require('.');

const TABLE = 'tracks';
const FIELDS = [
  'title',
  'artist',
  'isrc',
  'genre',
  'user_id',
  'album_id',
  'status',
];

class Track extends Model {}

const model = new Track({
  table: TABLE,
  fields: FIELDS,
});

module.exports = model;
