const Model = require('.');

const TABLE = 'albums';
const FIELDS = [
  'title',
  'artist',
  'label',
  'upc',
  'genre',
  'user_id',
  'status',
];

class Album extends Model {}

const model = new Album({
  table: TABLE,
  fields: FIELDS,
});

module.exports = model;
