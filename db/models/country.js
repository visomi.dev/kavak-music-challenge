const Model = require('.');

const TABLE = 'countries';
const FIELDS = [
  'code',
  'name',
];

class Country extends Model {}

const model = new Country({
  table: TABLE,
  fields: FIELDS,
});

module.exports = model;
