const utils = require('../utils');

async function up() {
  const sql = `
    INSERT INTO countries (id, code, name)
    VALUES
      (1, 'AR', 'Argentina'),
      (2, 'CO', 'Colombia'),
      (3, 'PE', 'Peru')
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DELETE FROM countries';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
