const utils = require('../utils');

async function up() {
  const sql = `
    INSERT INTO albums (id, title, artist, label, upc, genre, user_id, status)
    VALUES
      (10, 'To Hell', 'Cobra', 'Austral Holocaust Productions', 4250936511415, 'ROCK', 26, 1),
      (12, 'After', 'Vanilla Funk', 'Independiente', 634654350398, 'ROCK', 42, 1),
      (13, 'We Are Mercury', 'Mercury', 'All We Do Is Party', 634654350206, 'DANCE', 22, 1),
      (14, 'Amante', 'Desiré Mandrile', 'Lupa', 634654350060, 'R_B_SOUL', 24, 1),
      (142, 'Lethal Strike', 'Cobra', 'Austral Holocaust', 4250936511316, 'ROCK', 26, 1),
      (2146, 'Likeflow (Tropical Dance Music)', 'DJ Caile','Independiente', 634654352965, 'DANCE', 195, 1),
      (4587, 'Tormenta', 'Desire Mandrile', NULL, 634654356031, 'ALTERNATIVE', 24, 1),
      (8092, 'Indómitos', 'Desire Mandrile ', NULL, 634654363220, 'POP', 24, 1),
      (10479, 'Cumbia Space (Digital Cumbia 2017)', 'Mooglisound','Independiente', 634654367914, 'ELECTRONIC', 195, 1),
      (20840, 'Intensidad', 'Desire Mandrile', 'Independiente', 651973325548, 'LATIN', 24, 1)
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DELETE FROM albums';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
