const utils = require('../utils');

async function up() {
  const sql = `
    INSERT INTO tracks (id, title, artist, isrc, genre, album_id, user_id, status)
    VALUES
      (1,' Cuando pienso en ti', 'Desiré Mandrile Ballón', 'PEDM11403001', 'ROCK', 14, 24, 1),
      (2,' Lo que no fue no será', 'Desiré Mandrile Ballón', 'PEDM11403002', 'R_B_SOUL', 14, 24, 1),
      (3,' Ahora', 'Desiré Mandrile Ballón', 'PEDM11403003', 'R_B_SOUL', 14, 24, 1),
      (4,' Almohada', 'Desiré Mandrile Ballón', 'PEDM11403004', 'R_B_SOUL', 14, 24, 1),
      (5,' Cuando llegue la hora', 'Desiré Mandrile Ballón', 'PEDM11403005', 'R_B_SOUL', 14, 24, 1),
      (6,' Mío', 'Desiré Mandrile Ballón', 'PEDM11403006', 'R_B_SOUL', 14, 24, 1),
      (7,' Que somos amantes', 'Desiré Mandrile Ballón', 'PEDM11403007', 'R_B_SOUL', 14, 24, 1),
      (8,' Cóncavo y convexo', 'Desiré Mandrile Ballón', 'PEDM11403008', 'R_B_SOUL', 14, 24, 1),
      (9,' Aventurero', 'Desiré Mandrile Ballón', 'PEDM11403009', 'R_B_SOUL', 14, 24, 1),
      (10, 'Jamás impedirás', 'Desiré Mandrile Ballón', 'PEDM11403010', 'R_B_SOUL', 14, 24, 1),
      (32, 'El lagarto', 'Diego Reyes Bernardini', 'PEBQ91400305', 'ROCK', 12, 42, 1),
      (33, 'Ghostbusters', 'Diego Reyes Bernardini', 'PEBQ91400306', 'ROCK', 12, 42, 1),
      (34, 'Máncora', 'Diego Reyes Bernardini', 'PEBQ91400307', 'ROCK', 12, 42, 1),
      (35, 'Mario Bross', 'Diego Reyes Bernardini', 'PEBQ91400308', 'ROCK', 12, 42, 1),
      (36, 'Mingacho', 'Diego Reyes Bernardini', 'PEBQ91400309', 'ROCK', 12, 42, 1),
      (37, 'Strikeforce', 'Diego Reyes Bernardini', 'PEBQ91400310', 'ROCK', 12, 42, 1),
      (38, 'Payasito', 'Diego Reyes Bernardini', 'PEBQ91400311', 'ROCK', 12, 42, 1),
      (39, 'Beyond the Curse', 'Austral Holocaust Productions', 'PEBQ91400009', 'ROCK', 10, 26, 1),
      (40, 'Fallen Soldier', 'Austral Holocaust Productions', 'PEBQ91400010', 'ROCK', 10, 26, 1),
      (41, 'Danger Zone', 'Austral Holocaust Productions', 'PEBQ91400011', 'ROCK', 10, 26, 1),
      (42, 'Rough Riders', 'Austral Holocaust Productions', 'PEBQ91400012', 'ROCK', 10, 26, 1),
      (43, 'Beware My Wrath', 'Austral Holocaust Productions', 'PEBQ91400013', 'ROCK', 10, 26, 1),
      (44, 'When I Walk the Streets', 'Austral Holocaust Productions', 'PEBQ91400014', 'ROCK', 10, 26, 1),
      (45, 'To Hell', 'Austral Holocaust Productions', 'PEBQ91400015', 'ROCK', 10, 26, 1),
      (46, 'Inner Demon', 'Austral Holocaust Productions', 'PEBQ91400016', 'ROCK', 10, 26, 1),
      (47, 'Overwhelmed', 'Austral Holocaust Productions', 'PEBQ91400364', 'ROCK', 142, 26, 1),
      (48, 'Rockmetal', 'Austral Holocaust Productions', 'PEBQ91400365', 'ROCK', 142, 26, 1),
      (49, 'Men of War', 'Austral Holocaust Productions', 'PEBQ91400366', 'ROCK', 142, 26, 1),
      (50, 'Whitechapel', 'Austral Holocaust Productions', 'PEBQ91400367', 'ROCK', 142, 26, 1),
      (51, 'The Roadrunner (Bite my Dust)', 'Austral Holocaust Productions', 'PEBQ91400368', 'ROCK', 142, 26, 1),
      (52, 'Denim Attack', 'Austral Holocaust Productions', 'PEBQ91400369', 'ROCK', 142, 26, 1),
      (53, 'Blessed by Beer', 'Austral Holocaust Productions', 'PEBQ91400370', 'ROCK', 142, 26, 1),
      (54, 'Scene of Our End', 'Austral Holocaust Productions', 'PEBQ91400371', 'ROCK', 142, 26, 1),
      (55, 'Highland Warrior', 'Austral Holocaust Productions', 'PEBQ91400372', 'ROCK', 142, 26, 1),
      (70, 'Don\\'t You know Why', 'Felipe Gordon, Camilo Gómez, Leonardo La Rotta, Santiago Uribe, Juan Uribe', 'PEBQ91400046', 'DANCE', 13, 22, 1),
      (71, 'Going On (Ft Purple Zippers)', 'Felipe Gordon, Camilo Gómez, Leonardo La Rotta, Santiago Uribe, Juan Uribe', 'PEBQ91400047', 'DANCE', 13, 22, 1),
      (72, 'Jelly Beach', 'Felipe Gordon, Camilo Gómez, Leonardo La Rotta, Santiago Uribe, Juan Uribe', 'PEBQ91400048', 'DANCE', 13, 22, 1),
      (73, 'Right Now', 'Felipe Gordon, Camilo Gómez, Leonardo La Rotta, Santiago Uribe, Juan Uribe', 'PEBQ91400049', 'DANCE', 13, 22, 1),
      (77, 'Likeflow (Tropical Dance Music)', 'Joel Saez', 'PEBQ91502130', 'DANCE', 2146, 195, 1),
      (78, 'Cumbia Space (Digital Cumbia 2017)', NULL, 'PEBQ91610734', 'ELECTRONIC', 10479, 195, 1)
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DELETE FROM tracks';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
