const utils = require('../utils');

async function up() {
  const sql = `
    INSERT INTO users (id, name, email, country_id, status)
    VALUES
      (22, 'Felipe', 'mercury.musica@gmail.com', 2, 1),
      (24, 'Desire', 'desmandriba@yahoo.com', 3, 1),
      (26, 'Diego', 'cobra.rockmetal@gmail.com', 3, 1),
      (42, 'Diego', 'diegoreyes.bernardini@gmail.com', 3, 1),
      (183, 'Guillermo', 'guillermobruno@rocketmail.com', 1, 1),
      (195, 'Joel', 'djcaile@outlook.com', 2, 1)
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DELETE FROM users';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
