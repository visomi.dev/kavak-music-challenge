const utils = require('../utils');

async function up() {
  const sql = `
    create table tracks(
      id INT auto_increment PRIMARY KEY,
      title  VARCHAR(255),
      artist  VARCHAR(255),
      isrc  VARCHAR(255) UNIQUE,
      genre  VARCHAR(255),
      album_id INT,
      \`user_id\` INT,
      status INT,
      CONSTRAINT tracks_albums FOREIGN KEY (album_id) REFERENCES albums (id) ON UPDATE no action ON DELETE no action,
      CONSTRAINT traks_users FOREIGN KEY (\`user_id\`) REFERENCES users (id) ON UPDATE no action ON DELETE no action
    )
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DROP TABLE IF EXISTS tracks';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
