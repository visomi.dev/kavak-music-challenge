const connection = require('../connection');

const countries = require('./countries');
const users = require('./users');
const albums = require('./albums');
const tracks = require('./tracks');

const logger = require('../../lib/logger');

async function up() {
  try {
    await countries.up();
    await users.up();
    await albums.up();
    await tracks.up();

    connection.end();
  } catch (error) {
    logger.error(error);
    connection.end();
    process.exit(1);
  }
}

async function down() {
  try {
    await tracks.down();
    await albums.down();
    await users.down();
    await countries.down();

    connection.end();
  } catch (error) {
    logger.error(error);
    connection.end();
    process.exit(1);
  }
}

const migrations = {
  up,
  down,
};

module.exports = migrations;
