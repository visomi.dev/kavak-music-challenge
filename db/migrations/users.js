const utils = require('../utils');

async function up() {
  const sql = `
    create table users(
      id INT auto_increment PRIMARY KEY,
      name VARCHAR(100),
      email VARCHAR(50) unique,
      country_id INT,
      status INT,
      CONSTRAINT users_countries FOREIGN KEY (country_id) REFERENCES countries (id) ON UPDATE no action ON DELETE no action
    )
  `;

  const indexes = 'CREATE INDEX users_name ON users(name);';

  const result = await utils.query(sql);
  const resultIndexes = await utils.query(indexes);

  return [
    result,
    resultIndexes,
  ];
}

async function down() {
  const sql = 'DROP TABLE IF EXISTS users';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
