const utils = require('../utils');

async function up() {
  const sql = `
    create table albums(
      id INT auto_increment PRIMARY KEY,
      title VARCHAR(255),
      artist VARCHAR(255),
      label VARCHAR(255),
      upc BIGINT UNIQUE,
      genre VARCHAR(255),
      \`user_id\` INT,
      status INT,
      CONSTRAINT albums_users FOREIGN KEY (\`user_id\`) REFERENCES users (id) ON UPDATE no action ON DELETE no action
    )
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DROP TABLE IF EXISTS albums';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
