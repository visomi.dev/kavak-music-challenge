const utils = require('../utils');

async function up() {
  const sql = `
    CREATE TABLE countries(
      id INT auto_increment PRIMARY KEY,
      code  VARCHAR(50) UNIQUE,
      name  VARCHAR(255) UNIQUE
    )
  `;

  const result = await utils.query(sql);

  return result;
}

async function down() {
  const sql = 'DROP TABLE IF EXISTS countries';

  const result = await utils.query(sql);

  return result;
}

const migration = {
  up,
  down,
};

module.exports = migration;
