# KAVAK Music Challenge

## Requirements:

- [Latest Nodejs runtime](https://nodejs.org/es/download/)
- [MySQL](https://www.mysql.com/downloads/) or [MariaDB](https://mariadb.org/download/) server with user and password granted create tables to one database

## Instruccions:

### Get the repo:

```bash
git clone https://gitlab.com/visomi.dev/kavak-music-challenge.git
```

### Install the project:

```bash
cd kavak-music-challenge && npm install
```

### Configure:

In develop enviroment you can create .env file:

```bash
tee -a .env > /dev/null <<EOT
PORT = 8080
LOG_LEVEL = 'debug'
DB_HOST = 'localhost'
DB_USER = 'kavak-music-challenge'
DB_PASSWORD = 'kavak-music-challenge'
DB_NAME = 'kavak_music_challenge'
EOT
```

Or export the envs (this for production too):

```bash
export PORT=8080
export LOG_LEVEL='debug'
export DB_HOST='localhost'
export DB_USER='kavak-music-challenge'
export DB_PASSWORD='kavak-music-challenge'
export DB_NAME='kavak_music_challenge'
```

At the first time you need to create the tables:

```bash
npm run db migrate
```

If you want, you can seed the DB with some data:

```bash
npm run db seed
```

For undo the migrations (delete all tables):
```bash
npm run db migrate:undo
```

For undo the seeds (delete all rows):
```bash
npm run db seed:undo
```

## Run the project

In develop enviroment yoy can start the server with:
```bash
npm run dev
```

Or for production:
```bash
npm start
```

## Exercises

1. [http://localhost:8080/](http://localhost:8080/)
2. [http://localhost:8080/albums-from-peru](http://localhost:8080/albums-from-peru)
3. Go to the file `usecases/index.js:95`
4. [http://localhost:8080/empty-albums](http://localhost:8080/empty-albums)
5. [http://localhost:8080/empty-users](http://localhost:8080/empty-users)
5. [http://localhost:8080/tracks-genres](http://localhost:8080/tracks-genres)

## NOTES:

- If you want re create the API documentation run:
  ```bash
  npm run docs
  ```

- KavakMusicChallenge.postman_collection.json file contains all the CRUD requests, you can import from the app; need to set enviroment global API_URL to the URL server yoy want to test