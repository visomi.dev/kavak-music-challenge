const { set } = require('lodash');

const country = require('../db/models/country');

const helpers = require('../lib/helpers');

async function createOne({ code, name }) {
  const exists = await country.findOne({ $or: { code, name } });

  if (exists) {
    const error = new Error('country already exists');

    error.status = 409;

    throw error;
  }

  await country.createOne({ code, name });

  return country.findOne({ code, name });
}

async function findAll(query = {}) {
  const {
    page = 1,
    size = 20,
    sortBy,
    sortByDir,
    ...filters
  } = query;

  const search = Object.keys(filters).reduce((accum, key) => {
    set(accum, key, { $rlike: `${filters[key]}` });

    return accum;
  }, {});

  const [total, rows] = await Promise.all([
    country.count(search),
    country.findAll(search, { page, size }, { fields: sortBy, dir: sortByDir }),
  ]);

  return {
    total,
    pages: Math.ceil(total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

async function findById(id) {
  const result = await country.findOne({ id });

  if (!result) {
    const error = new Error('Country doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return result;
}

async function updateById(id, { code, name }) {
  const [exists, conflict] = await Promise.all([
    country.findOne({ id }),
    country.findOne({ id: { $different: id }, $or: { code, name } }),
  ]);

  if (!exists) {
    const error = new Error('Country doesn\'t exists');

    error.status = 404;

    throw error;
  }

  if (conflict) {
    const error = new Error('Country already exists');

    error.status = 409;

    throw error;
  }

  await country.updateOne({ id }, { code, name });

  return country.findOne({ id });
}

async function deleteById(id) {
  const result = await country.findOne({ id });

  if (!result) {
    const error = new Error('Country doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return country.deleteOne({ id });
}

const usecases = {
  createOne,
  findAll,
  findById,
  updateById,
  deleteById,
};

module.exports = usecases;
