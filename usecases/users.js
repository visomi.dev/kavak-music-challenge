const { set } = require('lodash');

const user = require('../db/models/user');

const helpers = require('../lib/helpers');

async function createOne(data) {
  const {
    name,
    email,
    countryId,
    status,
  } = data;

  const exists = await user.findOne({ email });

  if (exists) {
    const error = new Error('User already exists');

    error.status = 409;

    throw error;
  }

  await user.createOne({
    name,
    email,
    countryId,
    status: status ? 1 : 0,
  });

  const result = await user.findOne({ email });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function findAll(query = {}) {
  const {
    page = 1,
    size = 20,
    sortBy,
    sortByDir,
    ...filters
  } = query;

  const search = Object.keys(filters).reduce((accum, key) => {
    set(accum, key, { $rlike: `${filters[key]}` });

    return accum;
  }, {});

  const [total, rows] = await Promise.all([
    user.count(search),
    user.findAll(search, { page, size }, { fields: sortBy, dir: sortByDir }),
  ]);

  return {
    total,
    pages: Math.ceil(total / size),
    rows: helpers.camelCaseArrayOfObjects(rows).map(({ status, ...userData }) => ({
      ...userData,
      status: status === 1,
    })),
  };
}

async function findById(id) {
  const result = await user.findOne({ id });

  if (!result) {
    const error = new Error('User doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return {
    ...result,
    status: result.status === 1,
  };
}

async function updateById(id, data) {
  const {
    name,
    email,
    countryId,
    status,
  } = data;

  const [exists, conflict] = await Promise.all([
    user.findOne({ id }),
    user.findOne({ id: { $different: id }, email }),
  ]);

  if (!exists) {
    const error = new Error('User doesn\'t exists');

    error.status = 404;

    throw error;
  }

  if (conflict) {
    const error = new Error('User already exists');

    error.status = 409;

    throw error;
  }

  await user.updateOne({ id }, {
    name,
    email,
    countryId,
    status: status ? 1 : 0,
  });

  const result = await user.findOne({ id });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function deleteById(id) {
  const result = await user.findOne({ id });

  if (!result) {
    const error = new Error('User doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return user.deleteOne({ id });
}

const usecases = {
  createOne,
  findAll,
  findById,
  updateById,
  deleteById,
};

module.exports = usecases;
