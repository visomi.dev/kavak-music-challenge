const { set } = require('lodash');

const track = require('../db/models/track');

const helpers = require('../lib/helpers');

async function createOne(data) {
  const {
    title,
    artist,
    isrc,
    genre,
    albumId,
    userId,
    status,
  } = data;

  const exists = await track.findOne({ isrc });

  if (exists) {
    const error = new Error('Track already exists');

    error.status = 409;

    throw error;
  }

  await track.createOne({
    title,
    artist,
    isrc,
    genre,
    albumId,
    userId,
    status: status ? 1 : 0,
  });

  const result = await track.findOne({ isrc });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function findAll(query = {}) {
  const {
    page = 1,
    size = 20,
    sortBy,
    sortByDir,
    ...filters
  } = query;

  const search = Object.keys(filters).reduce((accum, key) => {
    set(accum, key, { $rlike: `${filters[key]}` });

    return accum;
  }, {});

  const [total, rows] = await Promise.all([
    track.count(search),
    track.findAll(search, { page, size }, { fields: sortBy, dir: sortByDir }),
  ]);

  return {
    total,
    pages: Math.ceil(total / size),
    rows: helpers.camelCaseArrayOfObjects(rows).map(({ status, ...trackData }) => ({
      ...trackData,
      status: status === 1,
    })),
  };
}

async function findById(id) {
  const result = await track.findOne({ id });

  if (!result) {
    const error = new Error('Track doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return {
    ...result,
    status: result.status === 1,
  };
}

async function updateById(id, data) {
  const {
    title,
    artist,
    isrc,
    genre,
    albumId,
    userId,
    status,
  } = data;

  const [exists, conflict] = await Promise.all([
    track.findOne({ id }),
    track.findOne({ id: { $different: id }, isrc }),
  ]);

  if (!exists) {
    const error = new Error('Track doesn\'t exists');

    error.status = 404;

    throw error;
  }

  if (conflict) {
    const error = new Error('Track already exists');

    error.status = 409;

    throw error;
  }

  await track.updateOne({ id }, {
    title,
    artist,
    isrc,
    genre,
    albumId,
    userId,
    status: status ? 1 : 0,
  });

  const result = await track.findOne({ id });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function deleteById(id) {
  const result = await track.findOne({ id });

  if (!result) {
    const error = new Error('Track doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return track.deleteOne({ id });
}

const usecases = {
  createOne,
  findAll,
  findById,
  updateById,
  deleteById,
};

module.exports = usecases;
