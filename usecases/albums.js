const { set } = require('lodash');

const album = require('../db/models/album');

const helpers = require('../lib/helpers');

async function createOne(data) {
  const {
    title,
    artist,
    label,
    upc,
    genre,
    userId,
    status,
  } = data;

  const exists = await album.findOne({ upc });

  if (exists) {
    const error = new Error('Album already exists');

    error.status = 409;

    throw error;
  }

  await album.createOne({
    title,
    artist,
    label,
    upc,
    genre,
    userId,
    status: status ? 1 : 0,
  });

  const result = await album.findOne({ upc });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function findAll(query = {}) {
  const {
    page = 1,
    size = 20,
    sortBy,
    sortByDir,
    ...filters
  } = query;

  const search = Object.keys(filters).reduce((accum, key) => {
    set(accum, key, { $rlike: `${filters[key]}` });

    return accum;
  }, {});

  const [total, rows] = await Promise.all([
    album.count(search),
    album.findAll(search, { page, size }, { fields: sortBy, dir: sortByDir }),
  ]);

  return {
    total,
    pages: Math.ceil(total / size),
    rows: helpers.camelCaseArrayOfObjects(rows).map(({ status, ...albumData }) => ({
      ...albumData,
      status: status === 1,
    })),
  };
}

async function findById(id) {
  const result = await album.findOne({ id });

  if (!result) {
    const error = new Error('Album doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return {
    ...result,
    status: result.status === 1,
  };
}

async function updateById(id, data) {
  const {
    title,
    artist,
    label,
    upc,
    genre,
    userId,
    status,
  } = data;

  const [exists, conflict] = await Promise.all([
    album.findOne({ id }),
    album.findOne({ id: { $different: id }, upc }),
  ]);

  if (!exists) {
    const error = new Error('Album doesn\'t exists');

    error.status = 404;

    throw error;
  }

  if (conflict) {
    const error = new Error('Album already exists');

    error.status = 409;

    throw error;
  }

  await album.updateOne({ id }, {
    title,
    artist,
    label,
    upc,
    genre,
    userId,
    status: status ? 1 : 0,
  });

  const result = await album.findOne({ id });

  return {
    ...result,
    status: result.status === 1,
  };
}

async function deleteById(id) {
  const result = await album.findOne({ id });

  if (!result) {
    const error = new Error('Album doesn\'t exists');

    error.status = 404;

    throw error;
  }

  return album.deleteOne({ id });
}

const usecases = {
  createOne,
  findAll,
  findById,
  updateById,
  deleteById,
};

module.exports = usecases;
