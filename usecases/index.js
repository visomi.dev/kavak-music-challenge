const dbutils = require('../db/utils');

const helpers = require('../lib/helpers');

async function tracksWithAlbumAndUser(queryParams = {}) {
  const { page = 1, size = 20 } = queryParams;

  const skip = (page - 1) * size;
  const limit = `${skip}, ${size}`;

  const countSql = `
    SELECT COUNT(*) AS total
    FROM tracks
    RIGHT JOIN albums ON tracks.album_id = albums.id
    RIGHT JOIN users ON tracks.user_id = users.id
    RIGHT JOIN countries ON users.country_id = countries.id
  `;

  const sql = `
    SELECT
      tracks.id as id,
      tracks.title as title,
      albums.title as album,
      users.email as email,
      countries.name as country
    FROM tracks
    RIGHT JOIN albums ON tracks.album_id = albums.id
    RIGHT JOIN users ON tracks.user_id = users.id
    RIGHT JOIN countries ON users.country_id = countries.id
    ORDER BY id DESC
    LIMIT ${limit}
  `;

  const [countRawResult, rows] = await Promise.all([
    dbutils.query(countSql),
    dbutils.query(sql),
  ]);

  const [countResult] = countRawResult;

  return {
    total: countResult.total,
    pages: Math.ceil(countResult.total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

async function albumsFromPeru(queryParams = {}) {
  const { page = 1, size = 20 } = queryParams;

  const skip = (page - 1) * size;
  const limit = `${skip}, ${size}`;

  const countSql = `
    SELECT COUNT(*) AS total
    FROM albums
    RIGHT JOIN users ON albums.user_id = users.id
    RIGHT JOIN countries ON users.country_id = countries.id
    WHERE countries.code = 'PE'
  `;

  const sql = `
    SELECT
      albums.id as id,
      albums.title as title,
      albums.artist as artist,
      albums.label as label,
      albums.upc as upc,
      albums.genre as genre,
      albums.user_id as user_idd,
      albums.status as status,
      countries.name as country
    FROM albums
    RIGHT JOIN users ON albums.user_id = users.id
    RIGHT JOIN countries ON users.country_id = countries.id
    WHERE countries.code = 'PE'
    ORDER BY id DESC
    LIMIT ${limit}
  `;

  const [countRawResult, rows] = await Promise.all([
    dbutils.query(countSql),
    dbutils.query(sql),
  ]);

  const [countResult] = countRawResult;

  return {
    total: countResult.total,
    pages: Math.ceil(countResult.total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

function fixTracksArtist() {
  const sql = `
    UPDATE tracks
    RIGHT JOIN users ON tracks.user_id = users.id
    SET tracks.artist = users.name
    WHERE tracks.artist IS NULL OR tracks.artist = ''
  `;

  return dbutils.query(sql);
}

async function getEmptyAlbums(queryParams) {
  const { page = 1, size = 20 } = queryParams;

  const skip = (page - 1) * size;
  const limit = `${skip}, ${size}`;

  const updateSql = `
    UPDATE albums
    SET status = 0
    WHERE id NOT IN (
      SELECT DISTINCT album_id
      FROM tracks
    )
  `;

  await dbutils.query(updateSql);

  const countSql = `
    SELECT COUNT(*) AS total
    FROM albums
    WHERE albums.status = 0
  `;

  const sql = `
    SELECT *
    FROM albums
    WHERE albums.status = 0
    ORDER BY id DESC
    LIMIT ${limit}
  `;

  const [countRawResult, rows] = await Promise.all([
    dbutils.query(countSql),
    dbutils.query(sql),
  ]);

  const [countResult] = countRawResult;

  return {
    total: countResult.total,
    pages: Math.ceil(countResult.total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

async function getEmptyUsers(queryParams) {
  const { page = 1, size = 20 } = queryParams;

  const skip = (page - 1) * size;
  const limit = `${skip}, ${size}`;

  const updateSql = `
    UPDATE users
    SET status = 0
    WHERE id NOT IN (
      SELECT DISTINCT user_id
      FROM albums
    ) OR id NOT IN (
      SELECT DISTINCT user_id
      FROM tracks
    )
  `;

  await dbutils.query(updateSql);

  const countSql = `
    SELECT COUNT(*) AS total
    FROM users
    WHERE users.status = 0
  `;

  const sql = `
    SELECT *
    FROM users
    WHERE users.status = 0
    ORDER BY id DESC
    LIMIT ${limit}
  `;

  const [countRawResult, rows] = await Promise.all([
    dbutils.query(countSql),
    dbutils.query(sql),
  ]);

  const [countResult] = countRawResult;

  return {
    total: countResult.total,
    pages: Math.ceil(countResult.total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

async function tracksGenres(queryParams = {}) {
  const { page = 1, size = 20 } = queryParams;

  const skip = (page - 1) * size;
  const limit = `${skip}, ${size}`;

  const countSql = `
    SELECT COUNT(*) AS total
    FROM tracks
    RIGHT JOIN albums ON tracks.album_id = albums.id
    WHERE tracks.genre != albums.genre
  `;

  const sql = `
    SELECT
      tracks.id as id,
      tracks.title as title,
      tracks.artist as artist,
      tracks.isrc as isrc,
      tracks.genre as genre,
      tracks.album_id as album_id,
      tracks.user_id as user_id,
      tracks.status as status
    FROM tracks
    RIGHT JOIN albums ON tracks.album_id = albums.id
    WHERE tracks.genre != albums.genre
    ORDER BY id DESC
    LIMIT ${limit}
  `;

  const [countRawResult, rows] = await Promise.all([
    dbutils.query(countSql),
    dbutils.query(sql),
  ]);

  const [countResult] = countRawResult;

  return {
    total: countResult.total,
    pages: Math.ceil(countResult.total / size),
    rows: helpers.camelCaseArrayOfObjects(rows),
  };
}

const usecases = {
  tracksWithAlbumAndUser,
  albumsFromPeru,
  fixTracksArtist,
  getEmptyAlbums,
  getEmptyUsers,
  tracksGenres,
};

module.exports = usecases;
