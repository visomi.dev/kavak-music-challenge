const fs = require('fs');

const readFileAsync = (path, options) => new Promise((resolve, reject) => {
  fs.readFile(path, options, (error, data) => {
    if (error) reject(error);
    else resolve(data);
  });
});

const writeFileAsync = (path, data, options) => new Promise((resolve, reject) => {
  fs.writeFile(path, data, options, (error) => {
    if (error) reject(error);
    else resolve();
  });
});

const unlinkAsync = (path) => new Promise((resolve, reject) => {
  fs.unlink(path, (error) => {
    if (error) reject(error);
    else resolve();
  });
});

const stat = (file) => new Promise((resolve, reject) => {
  fs.stat(file, (error, fileStat) => {
    if (error) reject(error);
    else resolve(fileStat);
  });
});

module.exports = {
  readFileAsync,
  writeFileAsync,
  unlinkAsync,
  stat,
};
