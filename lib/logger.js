const winston = require('winston');

const { LOG_LEVEL = 'debug' } = process.env;

const logger = winston.createLogger({
  level: LOG_LEVEL,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf(
      (entry) => `${entry.timestamp} ${entry.level}: ${entry.message}`,
    ),
  ),
  transports: [
    new winston.transports.Console(),
  ],
});

module.exports = logger;
