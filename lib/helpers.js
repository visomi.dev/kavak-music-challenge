const snakeCase = require('lodash/snakeCase');
const camelCase = require('lodash/camelCase');
const merge = require('lodash/merge');
const set = require('lodash/set');

const isArray = (array) => Array.isArray(array);
const isObject = (value) => typeof value === 'object' && !isArray(value);

function omitByDeep(obj, iteratee = (v) => !!v) {
  if (!obj) return obj;
  if (!isObject(obj)) return obj;

  const keys = Object.keys(obj);

  const cleanObj = keys.reduce((accum, key) => {
    let value = obj[key];

    if (isArray(value)) value = value.map((nestValue) => omitByDeep(nestValue, iteratee));
    if (isObject(value)) value = omitByDeep(value, iteratee);

    if (iteratee(value)) set(accum, key, value);

    return accum;
  }, {});

  return cleanObj;
}

function snakeCaseObject(obj) {
  if (!isObject(obj)) return obj;

  return Object.keys(obj).reduce(
    (accum, key) => merge(accum, { [snakeCase(key)]: obj[key] }),
    {},
  );
}

function camelCaseObject(obj) {
  if (!isObject(obj)) return obj;

  return Object.keys(obj).reduce(
    (accum, key) => merge(accum, { [camelCase(key)]: obj[key] }),
    {},
  );
}

function camelCaseArrayOfObjects(array) {
  if (!isArray(array)) return array;

  return array.map(camelCaseObject);
}

function camelCaseObjectDeep(obj) {
  if (!isObject(obj)) return obj;

  const keys = Object.keys(obj);

  const cleanObj = keys.reduce((accum, key) => {
    let value = obj[key];

    if (isArray(value)) value = value.map(camelCaseObjectDeep);
    if (isObject(value)) value = camelCaseObjectDeep(value);

    set(accum, snakeCase(key), value);

    return accum;
  }, {});

  return cleanObj;
}

function snakeCaseArrayOfObjects(array) {
  if (!isArray(array)) return array;

  return array.map(snakeCaseObject);
}

function snakeCaseObjectDeep(obj) {
  if (!isObject(obj)) return obj;

  const keys = Object.keys(obj);

  const cleanObj = keys.reduce((accum, key) => {
    let value = obj[key];

    if (isArray(value)) value = value.map(snakeCaseObjectDeep);
    if (isObject(value)) value = snakeCaseObjectDeep(value);

    set(accum, snakeCase(key), value);

    return accum;
  }, {});

  return cleanObj;
}

const helpers = {
  isObject,
  isArray,
  camelCaseObject,
  camelCaseArrayOfObjects,
  camelCaseObjectDeep,
  snakeCaseObject,
  snakeCaseArrayOfObjects,
  snakeCaseObjectDeep,
  omitByDeep,
  wait: (miliseconds) => new Promise((resolve) => setTimeout(resolve, miliseconds)),
  toCurrency(number = 0, currency = 'MXN', decimals) {
    let wrongValue = number;

    const formatterOptions = {
      style: 'currency',
      currency,
      maximumFractionDigits: decimals,
    };

    const formatter = new Intl.NumberFormat('es-MX', formatterOptions);

    wrongValue = parseFloat(wrongValue);

    if (Number.isNaN(wrongValue)) wrongValue = 0.00;

    const value = formatter.format(wrongValue);

    return value;
  },
};

module.exports = helpers;
