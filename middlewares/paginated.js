const TEN_BASE = 10;

function paginated(request, response, next) {
  const { size, page } = request.query;

  request.query.size = parseInt(size, TEN_BASE);
  request.query.page = parseInt(page, TEN_BASE);

  next();
}

module.exports = paginated;
