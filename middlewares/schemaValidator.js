const Ajv = require('ajv');
const get = require('lodash/get');

function validator(mainSchema, options = {}) {
  const { type = 'body', defsSchemas = [] } = options;

  const ajv = new Ajv({ allErrors: true });

  defsSchemas.forEach((schema) => { ajv.addSchema(schema); });

  const validate = ajv.compile(mainSchema);

  return (request, response, next) => {
    const data = get(request, type, {});

    const valid = validate(data);

    if (!valid) {
      response.status(400);
      response.json({
        success: false,
        message: 'Validation error, check the data for details',
        data: validate.errors,
      });
    } else {
      next();
    }
  };
}

module.exports = validator;
