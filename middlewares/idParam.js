const TEN_BASE = 10;

function idParam(request, response, next) {
  const { id } = request.params;

  request.params.id = parseInt(id, TEN_BASE);

  next();
}

module.exports = idParam;
