const path = require('path');

const express = require('express');
const morgan = require('morgan');

const countriesRoutes = require('./routes/countries');
const usersRoutes = require('./routes/users');
const albumsRoutes = require('./routes/albums');
const tracksRoutes = require('./routes/tracks');
const coreRoutes = require('./routes');

const { LOG_LEVEL = 'debug' } = process.env;

const app = express();
const format = LOG_LEVEL === 'debug' ? 'dev' : LOG_LEVEL;

app.set('views', './views');
app.set('view engine', 'pug');
app.use(express.json());
app.use(morgan(format));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/countries', countriesRoutes);
app.use('/users', usersRoutes);
app.use('/albums', albumsRoutes);
app.use('/tracks', tracksRoutes);
app.use('/', coreRoutes);

module.exports = app;
